
# coding: utf-8

# In[4]:

import numpy as np
import matplotlib.pyplot as plt
loaded_data = np.loadtxt("Data/data_1_var_2.txt", delimiter=",", unpack=True)
import statsmodels.api as sm
import seaborn as sns
import math
import pandas as pd


# In[5]:

variation_series = loaded_data

variation_series.sort()
n = len(variation_series)
print("вариационный ряд \n", variation_series)

mu = np.mean(variation_series)
print("выборочное среднее \n", mu)

print("выборочная дисперсия \n", np.var(variation_series))

sigma = np.var(variation_series)*n/(n-1)
print("исправленная дисперсия \n", sigma)
# / (1 + math.log(len(variation_series), 2))
swing = variation_series[-1] - variation_series[0]
delta_t = swing / (1 + math.log(len(variation_series), 2))
print("размах выборки \n", swing)

print("delta t \n", delta_t)
interval_amount = int(swing / delta_t) + 1
print("Количество интервалов \n", interval_amount)

df = pd.DataFrame()

for i in range(interval_amount):
    start = i * delta_t
    end = (i + 1) * delta_t
    values = variation_series[start < variation_series]
    values = values[end >= values]
    amount = len(values)
    df = df.append({'Начало':start, 'Конец': end, 'Середина': (i + 0.5) * delta_t, 'Частота': amount, 'Частотность': amount / len(variation_series) }, ignore_index=True)
print("Табоица 1 \n", df)
df2 = df
df2.loc[4, 'Частота'] = df2.loc[6, 'Частота'] + df2.loc[5, 'Частота'] + df2.loc[4, 'Частота']
df2.loc[4, 'Конец'] = df2.loc[6, 'Конец']
df2.loc[4, 'Середина'] = (df2.loc[4, 'Конец'] + df2.loc[4, 'Начало']) / 2
df2.loc[4, 'Частотность'] = df2.loc[4, 'Частота'] / len(variation_series)
df2 = df2.drop(df2.index[[5,6]])
print('Таблица 2 \n', df2)

a = (1/ len(variation_series)) * np.sum((df2['Середина'] * df2['Частота']))
print("a = ", a)

sigma_squared = (1/ len(variation_series)) * np.sum(((df2['Середина'] - a)**2 * df2['Частота'])) 
print("squared sigma = ", sigma_squared)

# In[6]:
mu = a
sigma = sigma_squared

n, bins, patches = plt.hist(variation_series, 20, facecolor='green', alpha=0.75, density=True)

plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) *
    np.exp(- (bins - mu)**2 / (2 * sigma**2)),
    linewidth=4, color='r')

plt._show()

# In[ ]:




# In[ ]:



